//! \file Sch.h

#pragma once

/**********************************************************
 *
 * Sch is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

typedef int TaskId_t;
typedef uint16_t delay_t;

struct TaskStruct
{
	void (*fp)(void);
	delay_t delay;
	delay_t period;
	uint8_t run;
};

typedef struct TaskStruct Task_t;

/******************************************************************************
 * Public function declarations
 *****************************************************************************/

void Sch_Create(void);
void Sch_Destroy(void);
TaskId_t Sch_Add_Task(void (*fp)(void), const delay_t delay, const delay_t period);
void Sch_Delete_Task(const TaskId_t id);
delay_t Sch_Get_Task_Delay(const TaskId_t id);
int Sch_Task_Ready(const TaskId_t id);
void Sch_Update(void);
void Sch_Dispatch_Tasks(void);
