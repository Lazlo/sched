//! \file SchTest.cpp

extern "C"
{
#include "Sch.h"
}

#include "CppUTest/TestHarness.h"

//
// Scheduler configuration and storage for task list
//

#define CONFIG_SCH_TASKS_MAX 3

unsigned char g_sch_tasks_max = CONFIG_SCH_TASKS_MAX;

Task_t g_task[CONFIG_SCH_TASKS_MAX];

//
// Fake task data and functions
//

static int fakeTask1_executed;

void fakeTask1(void)	{ fakeTask1_executed = 1; return; }
void fakeTask2(void)	{ return; }
void fakeTask3(void)	{ return; }
void fakeTask4(void)	{ return; }

TEST_GROUP(Sch)
{
	void setup()
	{
		fakeTask1_executed = 0;

		Sch_Create();
	}

	void teardown()
	{
		Sch_Destroy();
	}
};


// Init()
// Set_Tick_Periode()

//! \test Test Sch_Add_Task() will return task id of the task just added.
TEST(Sch, Add_Task_returns_task_id_on_success)
{
	TaskId_t id;
	TaskId_t id_expected;

	//--- Setup -----------------------------------------------------------

	// Prepare task id to be expected.
	// Since we are going to add three tasks to the task list,
	// the expected task id of the third task will be 3 (first task id is 1).
	id_expected = 3;

	//--- Execute ---------------------------------------------------------

	// Add the first two tasks (ignore returned task id).
	// NOTE: The arguments passed are not relevant in this test.
	Sch_Add_Task(fakeTask1, 10, 25);
	Sch_Add_Task(fakeTask2, 10, 25);

	// Add the third task and save the returned task id.
	id = Sch_Add_Task(fakeTask3, 10, 25);

	//--- Verify ----------------------------------------------------------

	// Verify returned task id of the third task.
	CHECK_EQUAL(id_expected, id);
}

//! \test Test Sch_Add_Task() will return error when the list of tasks is full.
TEST(Sch, Add_Task_returns_error_on_failure)
{
	TaskId_t id;
	TaskId_t id_expected;

	//--- Setup -----------------------------------------------------------

	// Prepare task id to be expected.
	id_expected = -1;

	//--- Execute ---------------------------------------------------------

	// Add the first three tasks (ignore returned task id).
	// NOTE: The arguments passed are not relevant in this test.
	Sch_Add_Task(fakeTask1, 0, 100);
	Sch_Add_Task(fakeTask2, 0, 100);
	Sch_Add_Task(fakeTask3, 0, 100);

	// Add the fourth task and save the returned task id.
	id = Sch_Add_Task(fakeTask4, 0, 500);

	//--- Verify ----------------------------------------------------------

	// Verify returned task id of the fourth task.
	CHECK_EQUAL(id_expected, id);
}

//! \test Test Sch_Get_Task_Delay() will return the current tasks delay.
//! \note This function is only relevant for test-driving the code.
//! It is not and should not be used in the scheduler itself.
TEST(Sch, Get_Task_Delay_retuns_task_delay)
{
	TaskId_t id;
	delay_t task_delay;
	delay_t task_delay_expected;

	//--- Setup -----------------------------------------------------------

	// Prepare the task delay to be expected.
	task_delay_expected = 10;

	//--- Execute ---------------------------------------------------------

	// Add a task.
	id = Sch_Add_Task(fakeTask1, task_delay_expected, 0);

	// Get the tasks delay value.
	task_delay = Sch_Get_Task_Delay(id);

	//--- Verify ----------------------------------------------------------

	// Verify returned task delay.
	CHECK_EQUAL(task_delay_expected, task_delay);
}

//! \test Test Sch_Task_Ready() will return zero when task is not ready to run.
//! \note This function is only relevant for test-driving the code.
//! It is not and should not be used in the scheduler itself.
TEST(Sch, Task_Ready_returns_zero_on_task_not_ready)
{
	TaskId_t id;
	int task_ready;
	int task_ready_expected;

	//--- Setup -----------------------------------------------------------

	task_ready_expected = 0;

	// Add task with delay 3 (hence will not be ready).
	id = Sch_Add_Task(fakeTask1, 3, 10);

	//--- Execute ---------------------------------------------------------

	task_ready = Sch_Task_Ready(id);

	//--- Verify ----------------------------------------------------------

	CHECK_EQUAL(task_ready_expected, task_ready);
}

//! \test Delete_Task()
// This test relies upon Sch_Add_Task() to allocate a new task in the first task list
// entry that is available (fp is non-zero) consequently reusing task list entries
// released by Sch_Delete_Task().
TEST(Sch, Delete_Task_releases_task_list_entry)
{
	TaskId_t id;
	TaskId_t id_expected;

	//--- Setup -----------------------------------------------------------

	// Prepare the expected task id (for the second task).
	id_expected = 2;

	// Add three tasks and save the task id of the second one.
	Sch_Add_Task(fakeTask1, 0, 15);
	id = Sch_Add_Task(fakeTask2, 0, 15);
	Sch_Add_Task(fakeTask3, 0, 15);

	//--- Execute ---------------------------------------------------------

	// Delete the second task.
	Sch_Delete_Task(id);

	// Add a new task and save its task id.
	id = Sch_Add_Task(fakeTask4, 0, 1);

	//--- Verify ----------------------------------------------------------

	// Verify returned task id (the new task should have the task id of the
	// task just deleted).
	CHECK_EQUAL(id_expected, id);
}

// Test Sch_Start() will ...

//! \test Test Sch_Update() will decrement a tasks delay value by one on every call.
TEST(Sch, Update_decrements_task_delay)
{
	TaskId_t id;
	delay_t task_delay;
	delay_t task_delay_returned;
	delay_t task_delay_expected;

	//--- Setup -----------------------------------------------------------

	// Prepare initial task delay to be passed when the task is added.
	task_delay = 5;

	// Prepare expected task delay (after three calls to Sch_Update()).
	task_delay_expected = 2;

	// Add a new task.
	id = Sch_Add_Task(fakeTask1, task_delay, 10);

	//--- Execute ---------------------------------------------------------

	// Call Sch_Update() 3 times.
	Sch_Update();
	Sch_Update();
	Sch_Update();

	// Get the current task delay.
	task_delay_returned = Sch_Get_Task_Delay(id);

	//--- Verify ----------------------------------------------------------

	// Verify returned task delay.
	CHECK_EQUAL(task_delay_expected, task_delay_returned);
}

//! \test Test Sch_Update() will reload the delay value with period value when
//! delay has reached zero.
TEST(Sch, Update_reloads_task_delay_with_period)
{
	TaskId_t id;
	delay_t task_delay;
	delay_t task_delay_returned;
	delay_t task_delay_expected;
	delay_t task_period;

	//--- Setup -----------------------------------------------------------

	// Prepare the expected task delay (after the delay reaches 0).
	task_delay_expected = 25;

	// Prepare delay and period values for a new task.
	task_delay = 0;
	task_period = 25;

	// Add a task and save its id for use with Sch_Get_Task_Delay().
	id = Sch_Add_Task(fakeTask1, task_delay, task_period);

	//--- Execute ---------------------------------------------------------

	// Expect Sch_Update() to decrement the task delay value until its zero
	// and then reload it with the tasks period value.
	Sch_Update();

	// Get the tasks current delay value.
	task_delay_returned = Sch_Get_Task_Delay(id);

	//--- Verify ----------------------------------------------------------

	// Verify returned task delay.
	CHECK_EQUAL(task_delay_expected, task_delay_returned);
}

//! \test Test Sch_Update() will set the task run attribute to non-zero when its delay is zero.
TEST(Sch, Update_marks_task_ready)
{
	TaskId_t id;
	int task_ready;
	int task_ready_expected;

	//--- Setup -----------------------------------------------------------

	task_ready_expected = 1;

	// Add a task will be marked ready on next call to Sch_Update()
	id = Sch_Add_Task(fakeTask1, 0, 5);

	//--- Execute ---------------------------------------------------------

	Sch_Update();

	task_ready = Sch_Task_Ready(id);

	//--- Verify ----------------------------------------------------------

	CHECK_EQUAL(task_ready_expected, task_ready);
}

//! \ŧest Test Sch_Dispatch_Tasks() will execute a task with run attribute non-zero.
TEST(Sch, Dispatch_Tasks_runs_task_with_run_attr_nonzero)
{
	//--- Setup -----------------------------------------------------------

	// Add a task with delay 0 so it will be marked ready on the next
	// call to Sch_Update().
	Sch_Add_Task(fakeTask1, 0, 10);

	//--- Execute ---------------------------------------------------------

	// Let Sch_Update() mark the task for execution.
	Sch_Update();
	// Let Sch_Dispatch_Tasks() execute the task.
	Sch_Dispatch_Tasks();

	//--- Verify ----------------------------------------------------------

	// Verify fakeTask1 has been executed.
	CHECK_EQUAL(1, fakeTask1_executed);
}

//! \test Test Sch_Dispatch_Tasks() will reset run attribute to zero.
TEST(Sch, Dispatch_Tasks_set_task_run_attr_to_zero)
{
	TaskId_t id;

	//--- Setup -----------------------------------------------------------

	// Add a task with delay 0 so it will be marked ready on the next
	// call to Sch_Update().
	id = Sch_Add_Task(fakeTask2, 0, 5);

	//--- Execute ---------------------------------------------------------

	// Let Sch_Update() mark the task for execution.
	Sch_Update();
	// Let Sch_Dispatch_Tasks() execute the task.
	Sch_Dispatch_Tasks();

	//--- Verify ----------------------------------------------------------

	// Verify the task is no longer ready after it has been executed.
	CHECK_EQUAL(0, Sch_Task_Ready(id));
}

//! \test Test Sch_Dispatch_Tasks() will delete single shot tasks.
TEST(Sch, Dispatch_Tasks_deletes_single_shot_task_after_execution)
{
	TaskId_t id;
	TaskId_t id_expected;

	//--- Setup -----------------------------------------------------------

	// Add three tasks, the 2nd one with a period of 0.
	Sch_Add_Task(fakeTask1, 0, 10);
	id_expected = Sch_Add_Task(fakeTask2, 0, 0);
	Sch_Add_Task(fakeTask3, 0, 25);

	// Let Sch_Update mark the task(s) for execution
	Sch_Update();

	//--- Execute ---------------------------------------------------------

	// Let Sch_Dispatch_Tasks() execute the tasks, and expect the single
	// shot one to be deleted from the task list upon return.
	Sch_Dispatch_Tasks();

	// Add a new task and expect its id to be equal to the single shot task.
	id = Sch_Add_Task(fakeTask2, 10, 5);

	//--- Verify ----------------------------------------------------------

	CHECK_EQUAL(id_expected, id);
}
