//! \file Sch.c

#include "Sch.h"

#define TASK_SET(t, f, d, p, r)			\
		do {				\
			t.fp		= f;	\
			t.delay		= d;	\
			t.period	= p;	\
			t.run		= r;	\
		} while (0);

#define TASK_INIT(t)	TASK_SET(t, 0, 0, 0, 0)
#define TASK_DEL	TASK_INIT

extern unsigned char g_sch_tasks_max;
extern Task_t g_task[];

void Sch_Create(void)
{
	TaskId_t ind;
	for (ind = 0; ind < g_sch_tasks_max; ind++)
		TASK_INIT(g_task[ind]);
}

void Sch_Destroy(void)
{
}

TaskId_t Sch_Add_Task(void (*fp)(void), const delay_t delay, const delay_t period)
{
	TaskId_t ind = 0;
	while ((g_task[ind].fp != 0) && (ind < g_sch_tasks_max))
		ind++;
	if (ind == g_sch_tasks_max)
		return -1;
	TASK_SET(g_task[ind], fp, delay, period, 0);
	return (ind+1);
}

void Sch_Delete_Task(const TaskId_t id)
{
	TaskId_t ind = id-1;
	TASK_DEL(g_task[ind]);
}

delay_t Sch_Get_Task_Delay(const TaskId_t id)
{
	TaskId_t ind = id-1;
	return g_task[ind].delay;
}

int Sch_Task_Ready(const TaskId_t id)
{
	TaskId_t ind = id-1;
	return g_task[ind].run;
}

void Sch_Update(void)
{
	TaskId_t ind;
	Task_t *t;
	for (ind = 0; ind < g_sch_tasks_max; ind++) {
		t = &g_task[ind];
		if (t->fp == 0)
			continue;
		if (t->delay == 0) {
			t->delay = t->period;
			t->run = 1;
		} else
			t->delay--;
	}
}

void Sch_Dispatch_Tasks(void)
{
	TaskId_t ind;
	Task_t *t;
	for (ind = 0; ind < g_sch_tasks_max; ind++) {
		t = &g_task[ind];
		if (t->fp == 0 || t->run == 0)
			continue;
		t->run = 0;
		(t->fp)();
		if (!t->period)
			TASK_DEL((*t));
	}
}
