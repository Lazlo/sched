#ifndef D_SchedBuildTime_H
#define D_SchedBuildTime_H

///////////////////////////////////////////////////////////////////////////////
//
//  SchedBuildTime is responsible for recording and reporting when
//  this project library was built
//
///////////////////////////////////////////////////////////////////////////////

class SchedBuildTime
  {
  public:
    explicit SchedBuildTime();
    virtual ~SchedBuildTime();
    
    const char* GetDateTime();

  private:
      
    const char* dateTime;

    SchedBuildTime(const SchedBuildTime&);
    SchedBuildTime& operator=(const SchedBuildTime&);

  };

#endif  // D_SchedBuildTime_H
