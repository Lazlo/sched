#include "SchedBuildTime.h"

SchedBuildTime::SchedBuildTime()
: dateTime(__DATE__ " " __TIME__)
{
}

SchedBuildTime::~SchedBuildTime()
{
}

const char* SchedBuildTime::GetDateTime()
{
    return dateTime;
}

